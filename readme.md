This repo contains three directories - 

- elasticsearch
- lambda
- terraform

# terraform directory
Terraform code, with a module for each component. 

Edit the file `main_variables.tf` to change the prefix and suffix of all resources that will be created, to add your user arn, and your public IP address. You can use https://ipecho.net/plain to find your IP address. 

**If you are connected to the VPN you IP may change frequently.**

Terraform also zips up the lambada and required node modules. 
The `esHost` in the lambda is set from an environment variable, all handled by the Terraform.

run the following commands - 
```
terraform init`
terraform plan -out plan.tfplan // examine the output to verify that it does what you want
terraform apply plan.tfplan
```


# elasticsearch directory
This directory contains a Node.js script to populate the ElasticSearch server with fake data. 

To use, set the `awsStreamName` to the correct Kinesis stream.
Edit the `casual.define` block to index documents, delete documents or both. The `casual.define` block also sets the ElasticSearch index name.

Edit the `for` loop to set a starting and ending document number.

# lambda directory
This is the Lambda code, it is rough and doesn't handle/log failures. 
But the Lambda is set to retry once.