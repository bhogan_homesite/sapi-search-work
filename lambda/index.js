'use strict'
const AWS = require('aws-sdk');
const createAwsElasticsearchConnector = require('aws-elasticsearch-connector')

const esHost = process.env.esHost;

require('array.prototype.flatmap').shim();
const { Client } = require('@elastic/elasticsearch');
const client = new Client({
    ...createAwsElasticsearchConnector(AWS.config),
    node: esHost
});

exports.handler = (event, context, callback) => {
    let itemsToIndex = [];
    let itemsToDelete = [];

    let counter = 0;
    if (event.Records != null) {
        console.log('event.Records.length ', event.Records.length);
        event.Records.forEach(record => {
            let payload = Buffer.from(record.kinesis.data, 'base64').toString('ascii');
            let obj = JSON.parse(payload);
            if (obj.action === 'index') {
                itemsToIndex.push(obj);
            }
            if (obj.action === 'delete') {
                itemsToDelete.push(obj);
            }
            counter++;
        });
        if(itemsToIndex.length > 0) {
            indexData(itemsToIndex);
        }
        if(itemsToDelete.length > 0) {
            deleteData(itemsToDelete);
        }
    }

    console.log("exiting from lambda, counter = " + counter);
    callback(null, "success, counter: " + counter);
};


async function indexData(items) {
    const body = items.flatMap(item => [{ index: { _index: item.partnerId, _id: item.id, _type: '_doc' } }, item.body]);
    console.log('index body ', JSON.stringify(body));

    const { body: bulkResponse } = await client.bulk({ refresh: true, body });
    bulkResponse.items.forEach((action, i) => {
    });
}

async function deleteData(itemDataset) {
    const body = itemDataset.flatMap(item => [{ delete: { _index: item.partnerId, _id: item.id, _type: '_doc' } }]);
    console.log('delete body ', JSON.stringify(body));
    
    const { body: bulkResponse } = await client.bulk({ refresh: true, body });
    bulkResponse.items.forEach((action, i) => {
        const operation = Object.keys(action)[0]

    });
   
}
