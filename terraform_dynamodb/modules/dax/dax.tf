resource "aws_dax_cluster" "dax_cluster" {
  cluster_name       = var.dax_cluster_name
  iam_role_arn       = var.dax_iam_role_arn
  node_type          = var.dax_node_type
  replication_factor = var.dax_replication_factor
}