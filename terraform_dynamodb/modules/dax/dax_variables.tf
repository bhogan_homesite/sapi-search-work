variable "dax_cluster_name"{
  type    = string
}

variable "dax_iam_role_arn" {
  type    = string
}

variable "dax_node_type"{
  type    = string
}

variable "dax_replication_factor"{
  type    = number
}
