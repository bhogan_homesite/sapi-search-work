output "dynamodb_id" {
  value = aws_dynamodb_table.dynamodb_table.id
}

output "dynamodb_arn" {
  value = aws_dynamodb_table.dynamodb_table.arn
}