resource "aws_iam_role_policy_attachment" "attach" {
  role       = aws_iam_role.dynamodb_permissions_role.name
  policy_arn = aws_iam_policy.dynamodb_permissions_policy.arn
}