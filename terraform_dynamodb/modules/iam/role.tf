resource "aws_iam_role" "dynamodb_permissions_role" {
  name = var.roleName
  assume_role_policy = var.roleBody
#   assume_role_policy = <<EOF
# {
#     "Version": "2012-10-17",
#     "Statement": {
#         "Effect": "Allow",
#         "Principal": { "AWS": "arn:aws:iam::673839138862:root" },
#         "Action": "sts:AssumeRole"
#     }
# }
# EOF
}

output "role_arn" {
  value = aws_iam_role.dynamodb_permissions_role.arn
}