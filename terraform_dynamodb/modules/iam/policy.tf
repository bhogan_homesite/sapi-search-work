resource "aws_iam_policy" "dynamodb_permissions_policy" {
  name        = var.policyName
  path        = "/"
  description = var.policyDescription
  policy = var.policyBody
}