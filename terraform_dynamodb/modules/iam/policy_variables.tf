variable "policyName" {
  type    = string
}

variable "policyDescription" {
  type    = string
}

variable "policyBody" {
  type    = string
}