provider "aws" {
  version = "~> 3.0"
  region  = "us-east-1"
}

module "readonly" {
  source = "./modules/iam"
  roleName = "${var.prefix}-dynamodb-readonly-access-role"
  roleBody = file("role-trust-policy.json")
  policyDescription = "A readonly policy for dynamodb tables"
  policyName = "${var.prefix}-dynamodb-readonly-access-policy"
  policyBody = file("dynamodb-readonly-access-policy.json")
}

output "readonly_role_arn" {
  value = module.readonly.role_arn
}

module "readwrite" {
  source = "./modules/iam"
  roleName = "${var.prefix}-dynamodb-readwrite-access-role"
  roleBody = file("role-trust-policy.json")
  policyDescription = "A readwrite policy for dynamodb tables"
  policyName = "${var.prefix}-dynamodb-readwrite-access-policy"
  policyBody = file("dynamodb-readwrite-access-policy.json")
}

output "readwrite_role_arn" {
  value = module.readwrite.role_arn
}

module "dax_role" {
  source = "./modules/iam"
  roleName = "${var.prefix}-dax-role"
  roleBody = file("dax-trust-policy.json")
  policyDescription = "A Dax policy for dynamodb tables"
  policyName = "${var.prefix}-dax-policy"
  policyBody = file("dax-access-policy.json")
}

output "dax_role_arn" {
  value = module.readwrite.role_arn
}



module "dax" {
  source = "./modules/dax"
  dax_iam_role_arn = module.dax_role.role_arn
  dax_cluster_name = "${var.prefix}-test"
  dax_node_type = "dax.t2.small"
  dax_replication_factor = 1
}

module "offerset_table" {
  source = "./modules/dynamodb"
  dynamodb_table_name               = "${var.prefix}-Offerset"                
  dynamodb_table_read_capacity      = 1
  dynamodb_table_write_capacity     = 1
  hash_key       = "OfferSetId"
  #range_key      = "PartnerKey"

  attributes = {
    "OfferSetId" = "S"
    #,"PartnerKey" = "S"
  }
}

module "questions_table" {
  source = "./modules/dynamodb"
  dynamodb_table_name               = "${var.prefix}-Questions"                
  dynamodb_table_read_capacity      = 1
  dynamodb_table_write_capacity     = 1
  hash_key       = "InquiryId"
  #range_key      = "Product"

  attributes = {
    "InquiryId" = "S"
    #,"Product" = "S"
  }
}

module "applicationquestions_table" {
  #source = "git@bitbucket.org:p20/dynamodb.git?ref=CD-2553"
  source = "./modules/dynamodb"
  dynamodb_table_name               = "${var.prefix}-ApplicationQuestions"                
  dynamodb_table_read_capacity      = 1
  dynamodb_table_write_capacity     = 1
  hash_key       = "ApplicationQuestionId"

  attributes = {
    "ApplicationQuestionId" = "S",
  }
}

module "choiceid_table" {
  source = "./modules/dynamodb"
  dynamodb_table_name               = "${var.prefix}-ChoiceID"                
  dynamodb_table_read_capacity      = 1
  dynamodb_table_write_capacity     = 1
  hash_key       = "ChoiceId"

  attributes = {
    "ChoiceId" = "S",
  }
}

module "taskstatus_table" {
  source = "./modules/dynamodb"
  dynamodb_table_name               = "${var.prefix}-TaskStatus"                
  dynamodb_table_read_capacity      = 1
  dynamodb_table_write_capacity     = 1
  hash_key       = "InquiryId"
  #range_key      = "TaskName"

  attributes = {
    "InquiryId" = "S"
    #,"TaskName" = "S"
  }
}

module "questionstatus_table" {
  source = "./modules/dynamodb"
  dynamodb_table_name               = "${var.prefix}-QuestionStatus"                
  dynamodb_table_read_capacity      = 1
  dynamodb_table_write_capacity     = 1
  hash_key       = "InquiryId"

  attributes = {
    "InquiryId" = "S",
  }
}



output "offerset_table_arn" {
  value = module.offerset_table.dynamodb_arn
}

output "questions_table_arn" {
  value = module.questions_table.dynamodb_arn
}

output "applicationquestions_table_arn" {
  value = module.applicationquestions_table.dynamodb_arn
}

output "choiceid_table_arn" {
  value = module.choiceid_table.dynamodb_arn
}

output "taskstatus_table_arn" {
  value = module.taskstatus_table.dynamodb_arn
}

output "questionstatus_table_arn" {
  value = module.questionstatus_table.dynamodb_arn
}
