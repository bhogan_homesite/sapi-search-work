resource "aws_kinesis_stream" "elasticsearch_stream" {
  name             = var.elasticsearch_stream_name
  shard_count      = var.elasticsearch_stream_shard_count
  retention_period = var.elasticsearch_stream_retention_period

  shard_level_metrics = [
    "IncomingBytes",
    "OutgoingBytes",
  ]
}