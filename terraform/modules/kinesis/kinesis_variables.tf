variable "elasticsearch_stream_name" {
  type    = string
}

variable "elasticsearch_stream_shard_count" {
  type = number
}

variable "elasticsearch_stream_retention_period"{
    type = number
}