output "elastic_search_arn" {
  value = aws_elasticsearch_domain.elasticsearch_domain.arn
}

output "elastic_search_endpoint" {
  value = aws_elasticsearch_domain.elasticsearch_domain.endpoint
}

