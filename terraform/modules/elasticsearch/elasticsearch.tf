
resource "aws_elasticsearch_domain" "elasticsearch_domain" {
  domain_name           = var.elasticsearch_domain_name
  elasticsearch_version = "7.7"

 ebs_options {
    ebs_enabled = var.ebs_enabled
    volume_type = var.volume_type
    volume_size = var.volume_size
  }

  cluster_config  {  
    instance_type             = var.instance_type
    instance_count            = var.instance_count
    dedicated_master_enabled  = var.dedicated_master_enabled
    dedicated_master_count    = var.dedicated_master_count
    dedicated_master_type     = var.dedicated_master_type
    zone_awareness_enabled    = var.zone_awareness_enabled 
    zone_awareness_config {
       availability_zone_count = var.availability_zone_count
  }  
  }

  # allowed_ips               = "[\"${var.user_public_ip_address}\"]" //we will need to update this
  # allowed_roles             = "[\"${module.my_iam_role.elastic_search_function_role_arn}\"]"
  

  
  node_to_node_encryption {
    enabled = var.node_to_node_encryption_enabled 
  }

  domain_endpoint_options {
    enforce_https = var.enforce_https 
    tls_security_policy = var.tls_security_policy
  }
  
  snapshot_options {
    automated_snapshot_start_hour = var.snapshot_options_automated_snapshot_start_hour 
  }

  encrypt_at_rest  {
    enabled    = var.enabled
    kms_key_id = var.kms_key_id
  }

  #we can enable this
  # log_publishing_options = {
    # enabled                  = "true"
    # log_type                 = "INDEX_SLOW_LOGS"
  # }

   tags = {
    owner = var.owner
    env   = var.env
  }

  access_policies = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": ${var.allowed_roles}
      },
      "Action": "es:*",
      "Resource": "arn:aws:es:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:domain/${var.elasticsearch_domain_name}/*"
    },
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "*"
      },
      "Action": "es:*",
      "Resource": "arn:aws:es:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:domain/${var.elasticsearch_domain_name}/*",
      "Condition": {
        "IpAddress": {
          "aws:SourceIp": ${var.allowed_ips}
        }
      }
    }
  ]
}
POLICY
}
