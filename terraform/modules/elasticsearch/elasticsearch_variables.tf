variable "elasticsearch_domain_name" {
  type    = string
}

variable "instance_type" {
  type = string
}

variable "ebs_enabled" {
  type    = bool
}

variable "volume_type" {
    type = string
}
variable "volume_size" {
    type = number
}

variable "instance_count" {
  type    = string
}

variable "zone_awareness_enabled" {
  type    = bool
}

variable "availability_zone_count" {
  type    = number
}

variable "dedicated_master_enabled" {
  type    = bool
}

variable "allowed_roles"{
    type = string
}

variable "allowed_ips"{
    type = string
}

//not needed RN as Lambda is not initialized
# variable "lambda_role_arn" {
#   type    = string
# }

variable "enabled" {
  type    = bool
}

variable "kms_key_id" {
  type    = string
}

variable "owner" {
  type    = string
}

variable "env" {
  type    = string
}

variable "node_to_node_encryption_enabled" {
  type    = bool
}

variable "snapshot_options_automated_snapshot_start_hour" {
  type    = string
}

variable "enforce_https" {
  type    = bool
}

variable "dedicated_master_count" {
  type    = number
}

variable "dedicated_master_type" {
  type    = string
}

variable "tls_security_policy" {
  type    = string
}

data "aws_region" "current" {}

data "aws_caller_identity" "current" {}
