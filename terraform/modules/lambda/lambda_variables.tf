variable "elastic_search_function_filename" {
  type    = string
}

variable "elastic_search_function_function_name" {
  type    = string
}

variable "elastic_search_function_role" {
  type    = string
}

variable "elastic_search_function_kinesis_event_source_arn" {
    type = string
}

variable "elastic_search_function_eshost" {
    type = string
}