resource "aws_lambda_function" "elastic_search_function" {
  filename      = var.elastic_search_function_filename
  function_name = var.elastic_search_function_function_name
  role          = var.elastic_search_function_role
  handler       = "index.handler"

  source_code_hash = filebase64sha256(var.elastic_search_function_filename)
  environment {
    variables = {
      esHost = "${var.elastic_search_function_eshost}"
    }
  }
  runtime = "nodejs12.x"
}

resource "aws_lambda_event_source_mapping" "elasticsearch_stream_to_lambda_trigger" {
  event_source_arn              = var.elastic_search_function_kinesis_event_source_arn
  function_name                 = aws_lambda_function.elastic_search_function.arn
  starting_position             = "LATEST"
  batch_size                    = 100
  maximum_retry_attempts        = 1
  maximum_record_age_in_seconds = 60
}

data "archive_file" "lambdafile" {
  type        = "zip"
  output_path = "${path.module}/lambda.zip"
  source_dir = "${path.root}/../lambda/"
}
