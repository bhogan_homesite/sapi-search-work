//devops needs to update this
//UPDATE ME
//this will create ESS with name of :  qa-sapi-elasticsearch
variable "prefix" {
  type    = string
  default = "qa"
}

variable "suffix" {
    type = string
    default = "v1"
}

//devops needs to update this
//UPDATE ME 
variable "user_arn"{
  type = string
  default = "arn:aws:sts::673839138862:assumed-role/hs-home-sandbox-user/rushabh.shah@homesite.com"
  # to get your arn run - okta-aws default sts get-caller-identity
  # need to update this
}

//devops needs to update this
//UPDATE ME 
variable "user_public_ip_address" {
  type = string
  default = "98.216.255.40"
  #need to update this
}

variable "instance_type" {
  type = string
  default = "m5.large.elasticsearch"
}

//devops needs to update this
//UPDATE ME 
variable "allowed_ips" {
  type = string
  default = "[\"104.129.194.197\", \"104.129.194.249\", \"199.189.178.1\", \"165.225.38.254\", \"108.26.161.28\"]" //need to update this by dev-ops
}

variable "volume_type" {
  type = string
  default = "gp2"
}

variable "volume_size" {
  type = number
  default = 10
}

variable "owner" {
  type = string
  default = "dev-ops"
}

//devops needs to update this
//UPDATE ME 
variable "env" {
  type = string
  default = "qa"
}


variable "instance_count" {
  type = number
  default = 3
}

variable "node_to_node_encryption_enabled" {
  type    = bool
  default = true
}

variable "snapshot_options_automated_snapshot_start_hour" {
  type    = string
  default = "23"
}

variable "enforce_https" {
  type    = bool
  default = true
}

variable "enabled" {
  type    = bool
  default = true
}

//devops needs to update this
//UPDATE ME 
variable "kms_key_id" {
  type    = string
  default = "alias/aws/es"  
}

variable "zone_awareness_enabled" {
  type    = bool
  default = true
}

variable "availability_zone_count" {
  type    = number
  default = 3
}

variable "dedicated_master_enabled" {
  type    = bool
  default = true
}

variable "ebs_enabled" {
  type    = bool
  default = true
}

variable "dedicated_master_count" {
  type    = number
  default = 3
}

//devops needs to update this
//UPDATE ME 
//not sure on this
variable "dedicated_master_type" {
  type    = string
  default = "m5.large.elasticsearch"
}

variable "tls_security_policy" {
  type    = string
  default = "Policy-Min-TLS-1-2-2019-07"
}