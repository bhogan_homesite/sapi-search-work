provider "aws" {
  version = "~> 3.0"
  region  = "us-east-1"
}

module "my_iam_role" {
  source                    = "./modules/roles"
  elastic_search_function_role_name = "${var.prefix}-elastic-search-function-role"
}

//We are commenting out kinessis instantiation as that is not required for R1
# module "my_kinesis_stream" {
#   source                    = "./modules/kinesis"
#   elasticsearch_stream_name = "${var.prefix}-elasticsearch-stream-${var.suffix}"
#   elasticsearch_stream_shard_count = 2
#   elasticsearch_stream_retention_period = 24
# }

//We are commenting out lambda instantiation as that is not required for R1
# module "my_lambda" {
#   source                    = "./modules/lambda"
#   elastic_search_function_filename = "./modules/lambda/lambda.zip"
#   elastic_search_function_function_name = "${var.prefix}-elastic-search-function-${var.suffix}"
#   elastic_search_function_role = module.my_iam_role.elastic_search_function_role_arn
#   elastic_search_function_kinesis_event_source_arn = module.my_kinesis_stream.elastic_search_stream_arn
#   elastic_search_function_eshost = "https://${module.my_elasticsearch_domain.elastic_search_endpoint}"
# }

//assigning variables for ESS
module "my_elasticsearch_domain" {
  source                              = "./modules/elasticsearch"
  elasticsearch_domain_name           = "${var.prefix}-sapi-elasticsearch"
  instance_type                       = "${var.instance_type}"
  allowed_roles                       = "[\"${module.my_iam_role.elastic_search_function_role_arn}\", \"${var.user_arn}\"]"
  allowed_ips                         = "${var.allowed_ips}"
  volume_type                         = "${var.volume_type}"
  volume_size                         = "${var.volume_size}"
  env                                 = "${var.env}"
  owner                               = "${var.owner}"
  instance_count                      = "${var.instance_count}"
  node_to_node_encryption_enabled     = "${var.node_to_node_encryption_enabled}"
  enforce_https                       = "${var.enforce_https}"
  enabled                             = "${var.enabled}"
  kms_key_id                          = "${var.kms_key_id}"
  zone_awareness_enabled              = "${var.zone_awareness_enabled}"
  availability_zone_count             = "${var.availability_zone_count}"
  dedicated_master_enabled            = "${var.dedicated_master_enabled}"
  ebs_enabled                         = "${var.ebs_enabled}"
  dedicated_master_count              = "${var.dedicated_master_count}"
  dedicated_master_type               = "${var.dedicated_master_type}"
  tls_security_policy                 = "${var.tls_security_policy}"
  snapshot_options_automated_snapshot_start_hour = "${var.snapshot_options_automated_snapshot_start_hour}"
}
