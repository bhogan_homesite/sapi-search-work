output "elasticsearch_arn" {
  value = module.my_elasticsearch_domain.elastic_search_arn
}

output "elasticsearch_endpoint" {
  value = module.my_elasticsearch_domain.elastic_search_endpoint
}
