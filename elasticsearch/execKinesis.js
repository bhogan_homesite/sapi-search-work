const AWS = require('aws-sdk');
const casual = require("casual");

const awsRegion = 'us-east-1';
const awsStreamName = 'bhogan-test-elasticsearch-stream-10';

casual.define('person', function () {
    return {
        id: 0,
        //action: 'delete',
        action: 'index',
        //action : casual.random_element(['index', 'delete']),
        partnerId:  'partner-a',
        //partnerId:  casual.random_element(['partner-a', 'partner-b', 'partner-c']),
        body: {
            firstName: casual.first_name,
            lastName: casual.last_name,
            email: casual.email,
            address: {
                addressLine1: casual.address1,
                addressLine2: casual.address2,
                city: casual.city,
                state: casual.state,
                zip: casual.zip(digits = 5)
            },
            phone: {
                number: casual.phone,
                type: casual.random_element(['Cell', 'Home', 'Office'])
            },
            policyType: casual.random_element(['Auto', 'Home']),
            age: casual.integer(from = 1, to = 99)
        }
    };
});

let kinesis = new AWS.Kinesis({ region: awsRegion });

for (let index = 1001; index <= 10000; index++) {
    let item = casual.person;

    item.id = index;
    sendToKinesis(item);
}

function sendToKinesis(item) {
    var recordParams = {
        Data: JSON.stringify(item),
        PartitionKey: item.id.toString(),
        StreamName: awsStreamName
    };

    kinesis.putRecord(recordParams, function (err, data) {
        if (err) {
            console.log('ERROR ' + err);
        }
        else {
            console.log(item.id + ' ' + item.partnerId +  ' ' + item.action + ' sent to Kinesis.');
        }
    });
}
